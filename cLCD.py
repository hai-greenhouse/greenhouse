import LCD1602
import I2C_LCD_driver
import time
from threading import Thread, Event
from clog import clog


class LCD16xx(Thread):

	def __init__(self, event):
		#ELCD1602.init(0x27, 1)   # init(slave address, background light)
		Thread.__init__(self)
		self._lumi = 0
		self._temp = 0
		self._humi = 0
		self._plcd = I2C_LCD_driver.lcd()
		self._mode = 1
		self.stopped = event
		self._interval = 120
		self.init()
		self._iLight = 'OFF'
		self._iHumi = 'OFF'
		self._iFan = 'OFF'
		self._iDoor = 'OFF'

	def init(self, addr=0x27, backlight=1):
		#line1 = "Init, Icon Testing"
		#self._plcd.lcd_display_string(line1, 1)
		self.NewIcon()
		#self.LightOnOff( 'ON' )
		#self.FanOnOff( 'ON' )
		#self.HumiOnOff( 'ON' )
		
		#time.sleep(5)

		#LCD1602.init(addr, backlight)	 # init(slave address, background light)
		#LCD1602.write(0, 0, 'LCD Init...')


	def conv2lumi(self, lumi):
		if lumi < 1:
			return '{:04.2f}'.format(lumi)

		if lumi < 100:
			return '{:04.1f}'.format(lumi)

		if lumi < 1000: # < 999
			return '{: 4d}'.format(int(lumi))

		if lumi < 10000:
			return '{:1.1f}K'.format((lumi/1000))

		return '{:03d}K'.format(int(lumi/1000))

	def setInterval(self, sec):
		self._interval = sec

	def wLight(self, lumi = 0):
		self._lumi = self.conv2lumi( lumi )
		self.rS1()

	def wTemp(self, temp = 0):
		self._temp = '{:02d}'.format(int(temp))
		self.rS1()

	def wHumi(self, humi = 0):
		self._humi = '{:02d}%'.format(int(humi))
		self.rS1()

	def rS0(self):
		line1 = "#hAI Greenhouse!"
		line2 = "                "
		self._plcd.lcd_display_string(line1, 1)
		self._plcd.lcd_display_string(line2, 2)

	def rIcon(self):
		self.rTime()
		self.LightOnOff( self._iLight )
		self.FanOnOff( self._iFan )
		self.HumiOnOff( self._iHumi )
		self.DoorOnOff( self._iDoor )
		return True

	def rS1(self):
		#self.clearlcd()
		line1 = "L:"+str(self._lumi)+"  T:"+str(self._temp)+"/"+str(self._humi)
		self._plcd.lcd_display_string(line1, 1)
		return True

	def run(self):
		while not self.stopped.wait( self._interval ):
			self.render()

	def rTime(self):
		time2 = str( time.strftime( '%H:%M', time.localtime() ) )
		self._plcd.lcd_display_string(time2, 2)
		return True

	def render(self):
		if self._mode == 1:
			self.CleanUP()
			self.rS1()			# Line 1
			self.rIcon()		# Line 2

	def show(self, word, line=1 ):
		try:
			self._plcd.lcd_display_string(word, line)
		except:
			print(" Except in cLCD, #1")

	def CleanUP(self):
		self._plcd.lcd_clear()

	def LightOnOff(self, OnOff):
		self._plcd.lcd_write(0xC6)
		self._iLight = OnOff
		if OnOff == 'ON':
			self._plcd.lcd_write_char(1)
			self._plcd.lcd_write_char(2)
		else:
			self._plcd.lcd_write_char(0)
			self._plcd.lcd_write_char(0)

	def FanOnOff(self, OnOff):
		self._plcd.lcd_write(0xC9)
		self._iFan = OnOff
		if OnOff == 'ON':
			self._plcd.lcd_write_char(3)
		else:
			self._plcd.lcd_write_char(0)

	def HumiOnOff(self, OnOff):
		self._plcd.lcd_write(0xCB)
		self._iHumi = OnOff
		if OnOff == 'ON':
			self._plcd.lcd_write_char(4)
		else:
			self._plcd.lcd_write_char(0)

	def DoorOnOff(self, OnOff):
		self._plcd.lcd_write(0xCD)
		self._iDoor = OnOff
		if OnOff == 'ON':
			self._plcd.lcd_write_char(4)
		else:
			self._plcd.lcd_write_char(0)


	def NewIcon(self):
		fontdata1 = [
		# char(0) - Upper-left character
			[ 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000 ],

		# char(1) - Upper-middle character
			[ 
			0b00000, 
			0b01111, 
			0b00011, 
			0b00101, 
			0b01001, 
			0b10000, 
			0b00000, 
			0b00000 ],
		
		# char(2) - Upper-right character
			[ 
			0b00000, 
			0b11110, 
			0b11000, 
			0b10100, 
			0b10010, 
			0b00001, 
			0b00000, 
			0b00000 ],
		
		# char(3) - Lower-left character
			[ 
			0b00000, 
			0b11111, 
			0b10101, 
			0b11111, 
			0b10101, 
			0b11111, 
			0b00000, 
			0b00000 ],
	   
		# char(4) - Lower-middle character
			[ 
			0b01010, 
			0b00100, 
			0b10010, 
			0b00100, 
			0b01101, 
			0b00110, 
			0b11111, 
			0b00000 ],
		
		# char(5) - Lower-right character
			[ 0b11000, 
			0b10000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000, 
			0b00000 ],
		]

		self._plcd.lcd_load_custom_chars(fontdata1)

		#self._plcd.lcd_write(0x80)
		#self._plcd.lcd_write_char(0)
		#self._plcd.lcd_write_char(1)
		
		#self._plcd.lcd_write(0xC0)
