import threading, time, json, re
from clog import clog
from websocket_server import WebsocketServer

# 光的執行模式
# 從資料庫的 Interval 中抓 cLight 的執行秒數，開/關燈後，要等 execLight 秒之後才會執行下次動作。

class cWebSocket(threading.Thread):
	def __init__(self, event):
		threading.Thread.__init__(self)
		self.stopped = event
		self._interval = 10

	def setInterval(self, seconds):
		self._interval = seconds

	def setObj(self, obj ):
		self._ob = obj

	def ws_message(self, client, ws, message):
		clog( " cWebSocket:ws_message,", message )
		if message == 'ask,lumi':
			lumi = self._ob['cLight'].getLumi()
			ws.send_message( client, str(lumi) )

		if message == 'ask,temp':
			temp = self._ob['cTemp'].getTempNow()
			ws.send_message( client, str( temp ) )

		if message == 'ask,humi':
			humi = self._ob['cHumi'].getHumiNow()
			ws.send_message( client, str( humi ) )

		if message == 'ask,lightauto':
			self._ob['cLight'].LightMode('auto')

		if message == 'ask,lightmanual':
			self._ob['cLight'].LightMode('manual')

		if message == 'ask,powerstatus':
			anspstatus = {}
			anspstatus['light'] = str(self._ob['cLight'].getPowerStatus())
			anspstatus['temp']	= str(self._ob['cTemp'].getPowerStatus())
			anspstatus['humi'] = str(self._ob['cHumi'].getPowerStatus())
			ws.send_message( client, json.dumps(anspstatus) )

		if message == 'ask,tempmanual':
			self._ob['cTemp'].TempMode('manual')

		if message == 'ask,tempauto':
			self._ob['cTemp'].TempMode('auto')

		if message == 'ask,humimanual':
			self._ob['cHumi'].HumiMode('manual')

		if message == 'ask,humiauto':
			self._ob['cHumi'].HumiMode('auto')

		if message == 'ask,lthmode':
			anspstatus = {}
			anspstatus['light'] = str(self._ob['cLight'].getLightMode())
			anspstatus['temp']	= str(self._ob['cTemp'].getTempMode())
			anspstatus['humi'] = str(self._ob['cHumi'].getHumiMode())
			ws.send_message( client, json.dumps(anspstatus) )

		if message == 'query,fanpower':
			fn = self._ob['cTemp'].getFanPower()
			ws.send_message( client, str(fn) )

		fp = re.match( "in,interval,({.*})", message )
		if fp:
			result = self._ob['dbh'].inInterval( str( fp.group(1) ) )
			ws.send_message( client, str( result ) )

		fp = re.match( "in,timeonoff,({.*})", message )
		if fp:
			result = self._ob['dbh'].inTimeOnOff( str( fp.group(1) ) )
			ws.send_message( client, str( result ) )

		fp = re.match( "ask,fanpower,(\d+)", message )
		if fp:
			self._ob['cTemp'].setFanPower( int(fp.group(1)) )

		fp = re.match( "query,ponoff,(\w+),(.*)", message, flags = re.I )
		if fp:
			result =  self.rPOnOffstr( fp.group(1), fp.group(2) )
			ws.send_message( client, json.dumps( result ) )

		fp = re.match( "in,note,(.*),(.*)", message )
		if fp:
			date = str( fp.group(1) )
			note = str( fp.group(2) )
			result = self._ob['dbh'].inNote( date, note )
			ws.send_message( client, result )
			
		fp = re.match( "query,note,(.*)", message )
		if fp:
			date = str( fp.group(1) )
			result = self._ob['dbh'].getNote( date )
			ws.send_message( client, result )


	def rPOnOffstr(self, item, qdate):
		# Convert Date from DB to string
		try:
			result = self._ob['dbh'].getPOnOff( item, qdate ) 
			strResult = []
			for item in result:
				strResult.append( str(item[2]) + " " + str(item[0]) + " " + str(item[1]) )
		except Exception as estr:
			clog( '[Error] cWebSocket:rPOnOffstr,', estr )
			return 'False'

		return strResult

	def ws_error(self, ws, error):
		clog( error )

	def ws_send(self, msg):
		try:
			self._ws.send_message_to_all( msg )
		except Exception as estr:
			clog( '[Error] cWebSocket:ws_send,', estr)

	def createServer(self):
		try:
			self._ws
			self._interval = 300
			clog(" [cWebSocket] createServer:OK WS ")
		except:
			clog(" [cWebSocket] Createing Server.... ")
			#websocket.enableTrace( True )
			self._ws = WebsocketServer( host='127.0.0.1', port=34567 )
			#self._ws.set_fn_new_client( self.ws_client )
			self._ws.set_fn_message_received( self.ws_message )
			wst = threading.Thread( target=self._ws.run_forever() )
			wst.daemon = False
			clog( " [cWebsocket] Server started" )
			wst.start()

	def run(self):
		while not self.stopped.wait(self._interval):
			self.createServer()
			clog( "  [cWebSocket] cWebSocket:run")
