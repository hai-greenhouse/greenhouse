import smbus
import time
from threading import Thread
import cLCD as lcd
from clog import clog

class MAX44009(Thread):

    _REG_INTERRUPT_STATUS = 0x00
    _REG_INTERRUPT_ENABLE = 0x01
    _REG_CONFIGURATION    = 0x02
    _REG_LUX_HIGH_BYTE    = 0x03
    _REG_LUX_LOW_BYTE     = 0x04
    _REG_UPPER_THRESHOLD  = 0x05
    _REG_LOWER_THRESHOLD  = 0x06
    _REG_TIMER_THRESHOLD  = 0x07

    ###########################
    # MAX44009 Code
    ###########################
    def __init__(self, event, bus=1, addr=0x4a):
        Thread.__init__(self)
        self.stopped = event
        self._bus = smbus.SMBus(bus)
        self._addr = addr
        self._lumi = 0
        self._interval = 600
        self._ob = None

    def _write(self, register, data):
        try:
            self._bus.write_byte_data(self._addr, register, data)
        except:
            if self._addr == 0x4a:
                self._addr = 0x4b
            else:
                self._addr = 0x4a
            self._bus.write_byte_data(self._addr, register, data)


    def _read(self, register):
        try:
            rdata = self._bus.read_byte_data(self._addr, register)
        except:
            if self._addr == 0x4a:
                self._addr = 0x4b
            else:
                self._addr = 0x4a
            print( "    [MAX44009] [Info] MAX44009:_read:addr has changed:", self._addr )
            rdata = self._bus.read_byte_data(self._addr, register)
        return rdata
        #return self._bus.read_byte_data(self._addr, register)

    def _read_block(self, register, size=2):
        try:
            rdata = self._bus.read_i2c_block_data(self._addr, register, size)
        except:
            if self._addr == 0x4a:
                self._addr = 0x4b
            else:
                self._addr = 0x4a
            print( "    [MAX44009] [Info] MAX44009:_read_block:addr has changed:", self._addr )
            rdata = self._bus.read_i2c_block_data(self._addr, register, size)
        return rdata


    def configure(self, cont=0, manual=0, cdr=0, timer=0):
        configuration = (cont & 0x01) << 7 | (manual & 0x01) << 6 | (cdr & 0x01) << 3 | timer & 0x07
        self._write(self._REG_CONFIGURATION, configuration)

    def setInterval(self, inter):
        self._interval = inter

    def setObj(self, obj):
        self._ob = obj

    def getNewLumi(self):
        self.luminosity()
        return self._lumi

    def firstrun(self):
        self.luminosity()
        self._ob['lcd'].wLight( self._lumi )

    def luminosity(self):
        data = self._read_block(self._REG_LUX_HIGH_BYTE, 2)
        exponent = (data[0] & 0xF0) >> 4
        mantissa = ((data[0] & 0x0F) << 4) | (data[1] & 0x0F)
        self._lumi = ((2 ** exponent) * mantissa) * 0.045
        return self._lumi

    def updateInterval(self, item = 'senLight'):
        try:
            sec = self._ob['dbh'].getInterval( item )
            if type( sec ) == int:
                self._interval = sec
                return sec
        except Exception as e:
            print("[MAX44009] [Error] updateInterval, '%s'" % item)

        return False


    def run(self):
        while not self.stopped.wait(self._interval):
            self.luminosity()
            clog( "  [MAX44009:run] Lumi: ", self._lumi )
            self.updateInterval()
            self._ob['lcd'].wLight( self._lumi )
            self._ob['dbh'].inLight( self._lumi )
