import adafruit_dht 
import time
from threading import Thread
from clog import clog

class DHT22(Thread):

	def __init__(self, event, PIN = 4):
		Thread.__init__(self)
		self.stopped = event
		self._pin = PIN
		self._humi = None
		self._temp = None
		self._DHT_SENSOR = adafruit_dht.DHT22( self._pin )
		self._gpio_pin = None
		self._interval = 600

	def setInterval(self, INTER = 600):
		self._interval = INTER

	def setObj(self, obj):
		self._ob = obj

	def setGPIO(self, PIN):
		self._gpio_pin = PIN

	def Read(self):
		self._humi = self._DHT_SENSOR.humidity
		self._temp = self._DHT_SENSOR.temperature
		return self._humi, self._temp

	def getHumi(self):
		if self._humi is None:
			self.Read()
		return self._humi

	def firstrun(self):
		self.Read()
		self._ob['lcd'].wTemp( self._temp )
		self._ob['lcd'].wHumi( self._humi )

	def getTemp(self):
		if self._temp is None:
			self.Read()
		return self._temp

	def updateInterval(self, item):
		try:
			sec = self._ob['dbh'].getInterval( item )
			if type( sec ) == int:
				self._interval = sec
				return sec
		except Exception as e:
			clog("[DHT22] [Error] updateInterval, item:" , item )
		return False

	def run(self):
		while not self.stopped.wait(self._interval):
			self.Read()
			clog( "  [DHT22:run] Temp:" ,self._temp , " Humi:", self._humi )
			#print( time.strftime( '%H:%M', time.localtime() ), "  [DHT22:run] Temp:", self._temp, " Humi:", self._humi )

			try:
				self._ob['dbh']
				self.WriteTemp2DB()
				self.WriteHumi2DB()
			except:
				clog( "[Error] DHT22:run, DBH not defined" )

			self._ob['lcd'].wTemp( self._temp )
			self._ob['lcd'].wHumi( self._humi )
			self.updateInterval( 'senTemp' )

	def WriteTemp2DB(self):
		clog( "  [DHT22] [Info] DHT22:WriteTemp2DB" )
		if self._temp is not None and self._ob['dbh'] is not None:
			self._ob['dbh'].inTemp( self._temp )

	def WriteHumi2DB(self):
		clog( "  [DHT22] [Info] DHT22:WriteHumi2DB")
		if self._humi is not None and self._ob['dbh'] is not None:
			self._ob['dbh'].inHumi( self._humi )

