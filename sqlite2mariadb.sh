#!/bin/bash
#
# Usage:
# ./sqlite2mariadb.sh sqlite.sql > mysql-format.sql
# cat mysql-format | mysql hAI -u root -p
#

cat $1 | sed '/PRAGMA foreign_keys=OFF;/d' | sed '/BEGIN TRANSACTION;/d' | sed 's/ AUTOINCREMENT/ AUTO_INCREMENT/gi' | sed "s/ (datetime(CURRENT_TIMESTAMP,'localtime'))/ CURRENT_TIMESTAMP/gi" | sed '/COMMIT;/d' | sed 's/Interval/`Interval`/gi' | sed '/sqlite_sequence/d'

#CREATE TABLE Temperature(id INTEGER PRIMARY KEY AUTOINCREMENT, temperature FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT, UUID varchar(128));
#INSERT INTO Temperature VALUES(1,22.999999999999998223,'2021-12-17 22:29:20',NULL,NULL);
#INSERT INTO Temperature VALUES(2,22.999999999999998223,'2021-12-17 22:30:21',NULL,NULL);
