import threading
import RPi.GPIO as GPIO
import time
from enum import Enum
from clog import clog

import TB6600
#
# 光的執行模式
# 從資料庫的 Interval 中抓 cLight 的執行秒數，開/關燈後，要等 cLight 秒之後才會執行下次動作。

class SensorLight(Enum):
	Above = 0
	Under = 1

class cLight(threading.Thread):
	def __init__(self, event, pin, objSensor):
		threading.Thread.__init__(self)
		self._pin = pin 
		self.stopped = event
		GPIO.setmode( GPIO.BCM )
		GPIO.setup( self._pin, GPIO.OUT )
		GPIO.output( self._pin, GPIO.HIGH )
		self._objsensor = objSensor
		self._sensorl = SensorLight.Under
		self._interval = 60
		self._lumiHigh = 500
		self._lumiLow = 300 
		self._powerstatus = 0
		self._lightmode = 0 # Auto
		self._objshade = TB6600.tb6600()

	def setSensorLight(self, L):
		self._sensorl = L

	def setGPIOMode(self, gpiomode):
		GPIO.setmode( gpiomode)

	def setInterval(self, seconds):
		self._interval = seconds

	def setObj(self, obj ):
		self._ob = obj

	def getHighLowfromDB(self, hour ):
		try:
			(self._lumiHigh, self._lumiLow, self._lightmode) = self._ob['dbh'].getLightOnOff( hour );
		except Exception as e:
			clog("[Error] [cLight] cLight:getHighLowfromDB, "+ str(e))

	def getLumi(self):
		try:
			self._lumi
		except:
			return 0

		return self._lumi

	def getPowerStatus(self):
		return self._powerstatus

	def getLightMode(self):
		return self._lightmode

	def ws_message(self, ws, message):
		def run( *args ):
			print( " cLight:ws_message,", message )
			if 'qlumi' in message:
				self.ws_send( 'alumi,' + str(self._lumi).encode('utf-8') )
				#self._ws.send( str(self._lumi) )

		threading.Thread(target=run).start()

	def ws_send(self, msg):
		if self._ob['Websocket']  == 0:
			return 1

		print( " cLight:ws_send,", msg )
		try:
			self._ob['ws'].ws_send( msg )
		except Exception as estr:
			clog( '[Error] cLight:ws_send Failed, ', estr)

	def LightMode(self, mode):
		if ( mode == 'manual' ):
			self._ob['dbh'].inLightMode( 1 )
			self._lightmode = 1
			self.PON()
		else:
			self._ob['dbh'].inLightMode( 0 )		# Auto
			self._lightmode = 0
			self.POFF()

	def PON(self, dontsave = None):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.LOW )
			clog( '  cLight:PON')
			if dontsave is None and self._ob['dbh']:
				self._ob['dbh'].inLightStatus( 'ON' )

			self._powerstatus = 1
			self._ob['lcd'].LightOnOff('ON')

			#try:
				#self._ob['ws'].ws_send('clight,pon')
			#except:
				#clog( " cLight:PON, websocket failed" )

	def POFF(self, dontsave = None):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.HIGH )
			clog( '  cLight:POFF')
			if dontsave is None and self._ob['dbh']:
				self._ob['dbh'].inLightStatus( 'OFF' )

			self._powerstatus = 0
			self._ob['lcd'].LightOnOff('OFF')

			#try:
				#self._ob['ws'].ws_send( 'clight,poff' )
			#except:
				#clog( " cLight:POFF, websocket failed" )


	def updateInterval(self, item = 'cLight' ):
		try:
			sec = self._ob['dbh'].getInterval( item )
			if type( sec ) == int:
				self._interval = sec
				return sec
		except Exception as e:
			self._interval = 600
			clog("[Error] [cLight] updateInterval" + str(e) )
		return False

	def firstrun(self):
		self._lumi = self._objsensor.getNewLumi()
		self._ob['lcd'].wLight( self._lumi )
		self._ob['lcd'].show( "Light Testing...", 1 )
		self.PON( dontsave = True )
		time.sleep(5)
		self.POFF( dontsave = True )
		#	self.updateInterval() # Do NOT update the interval at firstrun, let it go with 600s
		self.getHighLowfromDB( int(time.strftime( '%H', time.localtime() ) ) )	   # Now

	def setGPIO(self, PIN):
		self._pin = PIN

	def CleanUP(self):
		GPIO.cleanup()

	def run(self):
		while not self.stopped.wait(self._interval):
			clog( "  [cLight] cLight:run:Time:lightmode,", self._lightmode )
			self.updateInterval()

			if self._lightmode == 1:
				clog( "  [cLight] LightMode = 1 (Manually Power ON)" )
				if self._powerstatus == 0:
					self.PON()
					clog("   +---> But PowerStatus = 0, I turn ON the light" )
				continue

			pStatus = self._powerstatus
			if self._sensorl == SensorLight.Under and self._powerstatus == 1:
				self.POFF(dontsave = True)
				time.sleep(2)
		
			hour = int(time.strftime( '%H', time.localtime() ) )
			self.getHighLowfromDB( hour )	  # Now
			self._lumi = self._objsensor.getNewLumi()
			self._shadeon = self._ob['dbh'].getShadeOn( hour )

			clog( "  [cLight] run:Lumi:" , self._lumi," LumiOn:", self._lumiHigh, "-", self._lumiLow, " ShadeOn:", self._shadeon )

			if self._lumi < self._lumiLow or self._lumi  > self._lumiHigh:
				if self._powerstatus == 1: # if Sendor above Light, make a note for Turning off.
					self.POFF()
				else:
					clog( "  [cLight] [Info] run:exec POFF, but powerstatus:", self._powerstatus, ", do nothing")
					if pStatus == 1:	# if Sensor under Light, previosue status = 1 than make a note for Turning off.
						self.POFF()

			if self._lumi >= self._lumiLow and self._lumi < self._lumiHigh:
				if self._powerstatus == 0:
					#self.PON()
					if pStatus == 1:
						self.PON( dontsave = True )
					else:
						self.PON()
				else:
					clog( "  [cLight] [Info] run:exec PON but powerstatus:", self._powerstatus, ", do nothing")

			if self._lumi > self._shadeon:
				self._objshade.ShadeOn()
				self.PON()
				clog( "  [cLight] run:ShadeOn and cLight:PON" )
			else:
				self._objshade.ShadeOff()
				clog( "  [cLight] run:ShadeOff" )


