import RPi.GPIO as gpio
import time
 

class m28BYJ_48():
    def __init__(self):
        gpio.setmode(gpio.BCM)
        gpio.setwarnings(False)
        self._pin = [5,6,13,19]

        for i in range(4):
            gpio.setup(self._pin[i], gpio.OUT)
 
        self._forward_sq = ['0011', '1001', '1100', '0110']
        self._reverse_sq = ['0110', '1100', '1001', '0011']
 
    def forward(self, steps, delay):
        for i in range(steps):
            for step in self._forward_sq:
                self.set_motor(step)
                time.sleep(delay)
 
    def reverse(self, steps, delay):
        for i in range(steps):
            for step in self._reverse_sq:
                self.set_motor(step)
                time.sleep(delay)
    
    def set_motor(self, step):
        for i in range(4):
            gpio.output(self._pin[i], step[i] == '1')

#set_motor('0000')
#forward(180, 0.01)
#set_motor('0000')
#reverse(180,0.01)
