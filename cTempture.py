from threading import Thread, Event
import RPi.GPIO as GPIO
import time
from enum import Enum
from clog import clog

# 光的執行模式
# 每 30 分鐘抓一次光，達到標準就切換，切換後 30 分鐘不會動。

class SensorMode(Enum):
	Above = 0
	Under = 1

class cTemp(Thread):
	def __init__(self, event, pin, pwm_pin, objSensor):
		Thread.__init__(self)
		self._pin = pin 
		self._pwm_pin = pwm_pin
		self.stopped = event
		GPIO.setmode( GPIO.BCM )
		GPIO.setup( self._pin, GPIO.OUT )
		GPIO.setup( self._pwm_pin, GPIO.OUT )
		GPIO.output( self._pin, GPIO.HIGH )
		self._objsensor = objSensor
		self._interval = 600
		self._exectime = 120
		self._tempHigh = 22
		self._tempLow = 0
		self._powerstatus = 0
		self._tempMode = 0
		self._fanpower = 50
		self._pwm = GPIO.PWM( self._pwm_pin, 25 )
		self._maxfan = 100
		self._temp = None

	def getTempNow(self):
		if self._temp is None:
			self._temp = self._objsensor.getTemp()

		return self._temp

	def setTimeRange(self, start, end):
		self._timestart = start
		self._timeend = end

	def setONOFF(self, HIGH, LOW):
		self._tempHigh = HIGT
		self._tempLow = LOW

	def setGPIOMode(self, gpiomode):
		GPIO.setmode( gpiomode )

	def setPWNPIN(self, pwm_pin):
		self._pwm_pin = pwm_pin
		self._pwm = GPIO.PWM( pwm_pin, 25 )

	def setInterval(self, seconds):
		self._interval = seconds

	def setObj(self, obj):
		self._ob = obj

	def TempMode(self, mode='auto'):
		if mode == 'manual':
			self._ob['dbh'].inTempMode( 1 )
			self._tempMode = 1
			self.PON()
		else:
			self._ob['dbh'].inTempMode( 0 )
			self._tempMode = 0
			self._interval = 120
			self.POFF()

	def setFanPower(self, fanpower):
		self._fanpower = fanpower
		try:
			self._pwm
			self._pwm.ChangeDutyCycle( self._fanpower )
			clog( "  [cTemp] setFunPower,", self._fanpower )
		except:
			return '0'
		return '1'

	def getFanPower(self):
		return self._fanpower

	def getTempMode(self):
		return self._tempMode

	def getPowerStatus(self):
		return self._powerstatus

	def PON(self, first = None):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.LOW )
			clog( "  [cTemp] [Info] PON" )
			if first is None and self._ob['dbh'] and self._powerstatus == 0:
				self._ob['dbh'].inTempStatus('ON')

			self._powerstatus = 1
			self._ob['lcd'].FanOnOff('ON')

	def POFF(self, first = None):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.HIGH )
			self._pwm.start( self._maxfan )
			clog( "  [cTemp] [Info] POFF" )
			if first is None and self._ob['dbh'] and self._powerstatus == 1:
				self._ob['dbh'].inTempStatus('OFF')

			self._powerstatus = 0
			self._ob['lcd'].FanOnOff('OFF')

	def firstrun(self):
		self._ob['lcd'].show("Temp. testing...", 1)
		self.PON( first = True )
		time.sleep(5)
		self.POFF( first = True )
		self._temp = self._objsensor.getTemp()
		self.updateInterval()

	def getTempOnfromDB(self, hour):
		try:
			(self._tempHigh, self._tempMode) = self._ob['dbh'].getTempOn( hour )
		except Exception as e:
			clog("[Error] [cTemp] cTemp:getTempOnfromDB, %s" % str(e) )

	def setGPIO(self, PIN):
		self._pin = PIN

	def CleanUP(self):
		GPIO.cleanup()

	def updateInterval(self, item = 'cTemp'):
		try:
			sec = self._ob['dbh'].getInterval( item )
			if type( sec ) == int:
				self._interval = sec
				return sec
		except Exception as e:
			clog('[Error] [cTemp] [EupdateInterval' + str( e ) )
			self._interval = 600	# default

		self._interval = 600	# default
		return False

	def run(self):
		while not self.stopped.wait(self._interval):
			self._temp = self._objsensor.getTemp()
			self.getTempOnfromDB( int(time.strftime( '%H', time.localtime() )) ) # update _tempHigh
			clog( " [cTemp] run:Temperature:", self._temp, " TempOn:", self._tempHigh, " Interval:", self._interval )

			if ( self._tempMode == 1 ):
				clog( "  [cTemp] TempMode = 1 (Manual), Skip auto" )
				continue

			if self._powerstatus == 1:
				self.POFF()
				#sec = self._interval				 
				self.updateInterval( 'cTemp' )			# self._interval will update
				#self._interval = self._interval - sec
				#if self._interval == 0:
				clog(" [cTemp] [Info] updateInterval 'cTemp', next: %ds" % self._interval )
				continue

			if self._temp > self._tempHigh and self._powerstatus == 0:
				self.PON()
				self._fanpower = self._ob['dbh'].getFanPower( self._temp )
				clog( "  cTemp:Run:FanPower,", self._fanpower )
				if type( self._fanpower ) == int:
					self._pwm.ChangeDutyCycle( self._fanpower )

				self.updateInterval( 'execFan' )
				clog(" [cTemp] [Info] updateInterval 'execFan', interval:%ds" % self._interval )



