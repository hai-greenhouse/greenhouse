import time
from threading import Thread
import cLCD as lcd
from GreenPonik_BH1750.BH1750 import BH1750
from clog import clog

class BH_1750(Thread):

    def __init__(self, event, bus=1, addr=0x23):
        Thread.__init__(self)
        self.stopped = event
        self._addr = addr
        self._lumi = 0
        self._interval = 600
        self._ob = None
        self._bh = BH1750()

    def _write(self, register, data):
        return None

    def _read(self, register):
        return None
        #return self._bus.read_byte_data(self._addr, register)

    def _read_block(self, register, size=2):
        return None

    def configure(self, cont=0, manual=0, cdr=0, timer=0):
        configuration = (cont & 0x01) << 7 | (manual & 0x01) << 6 | (cdr & 0x01) << 3 | timer & 0x07
        return None

    def setInterval(self, inter):
        self._interval = inter

    def setObj(self, obj):
        self._ob = obj

    def getNewLumi(self):
        self.luminosity()
        return self._lumi

    def firstrun(self):
        self.luminosity()
        self._ob['lcd'].wLight( self._lumi )

    def luminosity(self):
        self._lumi = self._bh.read_bh1750()
        return self._lumi

    def updateInterval(self, item = 'senLight'):
        try:
            sec = self._ob['dbh'].getInterval( item )
            if type( sec ) == int:
                self._interval = sec
                return sec
        except Exception as e:
            clog("[BH1750] [Error] updateInterval, '%s'" % item)

        return False


    def run(self):
        while not self.stopped.wait(self._interval):
            self.luminosity()
            clog( time.strftime('%H:%M', time.localtime()), "  [BH1750:run] Lumi: ", self._lumi )
            self.updateInterval()
            self._ob['lcd'].wLight( self._lumi )
            self._ob['dbh'].inLight( self._lumi )
