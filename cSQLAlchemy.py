from flask import Flask
from sqlalchemy import Column, Integer, String, Float, DateTime, create_engine, func, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import QueuePool

from datetime import datetime
import json, time, re
from clog import clog

Base = declarative_base()

class cDB():

	#_engine = create_engine('sqlite+pysqlite:///:memory:', echo = True )
	_engine = create_engine('sqlite:////home/hAI/data.db?check_same_thread=False', echo = False, poolclass = QueuePool )
	#_engine = create_engine('mariadb+pymysql://root:hai-greenhouse@localhost/hAI?charset=utf8mb4', echo = False )

	def __init__(self):
		Base.metadata.create_all(self._engine)
		self.getSession()
		# https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_declaring_mapping.htm

		
	def getSession(self):
		self._Session = sessionmaker( bind = self._engine )
		self._session = self._Session() 
		return self._session
		#return Session(self._engine)

	def isnum(self, t):
		try:
			x = float(t)
			return True
		except:
			return False

	def inInterval(self, jdata):
		field = json.loads( jdata )
		clog( ' [cDB] inInterval,', field )
		try:
			for item in ( 'execFan', 'execHumi', 'cLight', 'cTemp', 'cHumi', 'senLight', 'senTemp', 'senHumi', 'cDoor' ):
				if self.isnum( field[item] ):
					self._session.query( Interval ).filter( Interval._item == item ).update( {Interval._seconds: int(field[item])} )					
					clog( ' [cDB] inInterval:', item, ":seconds,", field[item] )

			self._session.commit()
			return 'True'
		except Exception as e:
			clog( ' [cDB] inInterval:error,', str(e) )
			return 'False'

	def inTimeOnOff(self, jdata):
		field = json.loads( jdata )
		clog( ' [cDB] inTimeOnOff,jdata', field )
		try:
			hour24h = int( field['hour24h'] )
			if hour24h < 0 or hour24h > 23:
				clog( ' [cDB] inTimeOnOff:Error:Hour24h not 0 - 23,', str( field['hour24h'] ) )
				return 'False'

			if self.isnum( field['lighton'] ):
				lighton = int( field['lighton'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._lighton:lighton} )
				clog( ' [cDB] inTimeOnOff:lighton,', field['lighton'] )

			if self.isnum( field['lightoff'] ):
				lightoff = int( field['lightoff'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._lightoff:lightoff} )
				clog( ' [cDB] inTimeOnOff:lightoff,', field['lightoff'] )

			if self.isnum( field['shadeon'] ):
				shadeon = int( field['shadeon'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._shadeon:shadeon} )
				clog( ' [cDB] inTimeOnOff:shadeon,', field['shadeon'] )

			if self.isnum( field['tempon'] ):
				tempon = float( field['tempon'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._tempon:tempon} )
				clog( ' [cDB] inTimeOnOff:tempon,', field['tempon'] )

			if self.isnum( field['humion'] ):
				humion = float( field['humion'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._humion:humion} )
				clog( ' [cDB] inTimeOnOff:humion,', field['humion'] )

			if self.isnum( field['dooronoff'] ):
				dooronoff = int( field['dooronoff'] )
				self._session.query( TimeOnOff ).filter( TimeOnOff._hour24h == hour24h ).update( {TimeOnOff._dooronoff:dooronoff} )
				clog( ' [cDB] inTimeOnOff:dooronoff,', field['dooronoff'] )

			self._session.commit()
			return 'True'

		except Exception as e:
			clog( ' cDB:inTimeOnOff:except', str(e) )
			return 'False' 


	def inTemp(self, value, now = None):
		try:
			if now is None:
				otemp = Temp( value )
			else:
				otemp = Temp( value, curdate = now )

			self._session.add( otemp )
#			self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inTemp," + e )

	def inHumi(self, value, now = None):
		try:
			if now is None:
				ohumi = Humi( value )
			else:
				ohumi = Humi( value, now )

			self._session.add( ohumi )
			#self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inTemp," + e )

	def inLight(self, value, now = None):
		try:
			if now is None:
				olumi = Light( value )
			else:
				olumi = Light( value, now )

			self._session.add( olumi )
			self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inLight," + e )

	def inLightStatus(self, value):
		try:
			oponoff = POnOff( 'Light', value )
			self._session.add( oponoff )
			#self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inLightStatus," + e )

	def inTempStatus(self, value):
		try:
			oponoff = POnOff( 'Temperature', value )
			self._session.add( oponoff )
			#self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inTempStatus," + e )

	def inHumiStatus(self, value):
		try:
			oponoff = POnOff( 'Humidity', value )
			self._session.add( oponoff )
			#self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inHumiStatus," + str(e) )

	def inLightMode(self, mode=1):
		try:
			self._session.query( TimeOnOff ).update({TimeOnOff._lightmode:mode})
			self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inLightMode," + str(e) )

	def inTempMode(self, mode=0):
		try:
			self._session.query( TimeOnOff ).update({TimeOnOff._tempmode:mode})
			self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inTempMode," + str(e) )

	def inHumiMode(self, mode=0):
		try:
			self._session.query( TimeOnOff ).update({TimeOnOff._humimode:mode})
			self._session.commit()
		except Exception as e:
			clog( "[Error] cDB:inHumiMode," + str(e) )

	def getFanPower(self, Temp):
		Temp = int( Temp )
		try:
			result = self._session.query( FanPower._fanpower ).filter( FanPower._temp < Temp ).order_by( FanPower._fanpower.desc() ).first()
			return result._fanpower
		except Exception as e:
			clog( "[Error] in GetFanPower,", e )

		return 50

	def getShadeOn(self, hour):
		try:
			result = self._session.query( TimeOnOff._shadeon ).filter( TimeOnOff._hour24h == hour ).first()
			ShadeOn = result._shadeon
		except Exception as e:
			clog( "[Error] cDB:getShadeOn," + e )
			return 9999

		return ShadeOn

	def getLightOnOff(self, hour):
		#sql = "select `LightOff`,`LightOn`,`LightMode` from TimeOnOff where `Hour24H` = %d" % hour
		try:
			result = self._session.query( TimeOnOff._lightoff, TimeOnOff._lighton, TimeOnOff._lightmode ).filter( TimeOnOff._hour24h == hour ).first()
			LightOff = result._lightoff
			LightOn = result._lighton
			LightMode = result._lightmode
		except Exception as e:
			clog( "[Error] cDB:getLightOnOff," + e )
			return(-9999, -999 , 1)

		return (LightOff, LightOn, LightMode)

	def getTempOn(self, hour):
		try:
			result = self._session.query( TimeOnOff._tempon, TimeOnOff._tempmode ).filter( TimeOnOff._hour24h == hour ).first()
			TempOn = result._tempon
			TempMode = result._tempmode
		except Exception as e:
			clog( "[Error] cDB:getTempOn," + e )
			return (-999, 1)

		return (TempOn, TempMode)

	def getHumiOn(self, hour):
		try:
			result = self._session.query( TimeOnOff._humion, TimeOnOff._humimode ).filter( TimeOnOff._hour24h == hour ).first()
			HumiOn = result._humion
			HumiMode = result._humimode
		except Exception as e:
			clog( "[Error] cDB:getHumiOn," + e )
			return (-999, 1)

		return (HumiOn, HumiMode)

	def getDoorOnOff(self, hour):
		try:
			result = self._session.query( TimeOnOff._dooronoff ).filter( TimeOnOff._hour24h == hour ).first()
			DoorOnOff = result._dooronoff
		except Exception as e:
			clog( "[Error] cDB:getHumiOn," + e )
			return 0

		return DoorOnOff

	def getInterval(self, item):
		try:
			result = self._session.query( Interval._seconds ).filter( Interval._item == item ).first()
			seconds = result._seconds
		except Exception as e:
			clog( "[Error] cDB:getInterval," + str(e) )
			return 900
		
		return seconds

	def getSettings(self, item):
		try:
			result = self._session.query( Settings._setting ).filter( Settings._item == item ).first()
			setting = result._setting
		except Exception as e:
			clog( "[Error] cDB:getInterval," + str(e) )
			return False
		
		return setting

	def getPOnOff(self, item, qdate):
		checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', qdate )
		if not checkdate:
			qdate = time.strftime( '%Y-%m-%d', time.localtime() )

		if item is not None and item not in ('Light', 'Temperature', 'Humidity'):
			return ''

		try:
			if item is not None:
				result = self._session.query( POnOff._item, POnOff._pstatus, POnOff._curdate ).filter( POnOff._item == item, POnOff._curdate.like(qdate+'%') ).order_by( POnOff._curdate.asc() ).all()
			else:
				result = self._session.query( POnOff._item, POnOff._pstatus, POnOff._curdate ).filter( POnOff._curdate.like(qdate+'%') ).order_by( POnOff._curdate.asc() ).all()

			return result
		except Exception as e:
			clog( "[Error] cDB:getPOnOff," + str(e) )
			return False

	def getNote(self, qdate):
		checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', qdate )
		if not checkdate:
			qdate = time.strftime( '%Y-%m-%d', time.localtime() )

		clog( " cDB:getNote:qdate,", qdate )
		try:
			result = self._session.query( Note._note ).filter( Note._curdate.like( qdate+"%" ) ).order_by( Note._curdate.asc() ).first()
			return result._note
		except Exception as e:
			clog( "[Error] cDB:getNote," + str(e) )

		return ''

	def inNote( self, qdate = None, note = '' ):
		checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', qdate )
		if not checkdate:
			qdate = time.strftime( '%Y-%m-%d', time.localtime() )

		clog( " cDB:inNote:qdate,", qdate, " note:", note )
		try:
			if self._session.query( Note ).filter( Note._curdate.like(qdate+'%') ).count() == 0:
				clog( " cDB:inNote:count:0, make new note" )
				qdate = datetime.strptime( qdate , "%Y-%m-%d" )
				newnote = Note( note = note, curdate = qdate )
				result = self._session.add( newnote )
				self._session.commit()
			else:
				clog( " cDB:inNote:updatenote:", qdate, ",note:", note )
				result = self._session.query( Note ).filter( Note._curdate.like(qdate+'%') ).update( {Note._note: str(note) }, synchronize_session=False )
				self._session.commit()

			return 'OK'
		except Exception as e:
			clog( '[Error] cDB:inNote,', str(e) )
			return False

class TimeOnOff(Base):

# CREATE TABLE TimeOnOff(id INTEGER PRIMARY KEY AUTOINCREMENT, Hour24H INTEGER, LightOff INTEGER, LightOn INTEGER, TempOff INTEGER, TempOn INTEGER, HumiOff INTEGER, HumiOn INTEGER, UUID TEXT, `DoorOnOff` INTEGER, LightMode integer, TempMode integer, HumiMode integer);
	__tablename__ = 'TimeOnOff'
	_id = Column('id', Integer, primary_key = True)
	_hour24h = Column( 'Hour24H', Integer )
	_lightmode = Column( 'LightMode', Integer )
	_lightoff = Column( 'LightOff', Integer )
	_lighton = Column( 'LightOn', Integer )
	_tempmode = Column( 'TempMode', Integer )
	_tempoff = Column( 'TempOff', Integer )
	_tempon = Column( 'TempOn', Integer )
	_humimode = Column( 'HumiMode', Integer )
	_humioff = Column( 'HumiOff', Integer )
	_humion = Column( 'HumiOn', Integer )
	_dooronoff = Column( 'DoorOnOff', Integer )
	_shadeon = Column( 'ShadeOn', Integer )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, hour24, lmode, lon, loff, tmode, ton, toff, hmode, hon, hoff, door, shadeon, uuid):
		self._hour24 = hour24
		self._lightmode = lmode
		self._lighton = lon
		self._lightoff = loff
		self._tempmode = tmode
		self._tempon = ton
		self._tempoff = toff
		self._humimode = hmode
		self._humion = hon
		self._humioff = hoff
		self._dooronoff = door
		self._shadeon = shadeon
		self._uuid = uuid

class Light(Base):
	__tablename__ = 'Light'
#CREATE TABLE Light(id INTEGER PRIMARY KEY AUTOINCREMENT, light FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);
	_id = Column('id', Integer, primary_key = True)
	_light = Column('Light', Float)
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column('UUID', String(128))

	def __init__(self, Light, curdate = None, uuid = ''):
		self._light = Light
		self._curdate = curdate
		self._uuid = uuid

class Interval(Base):
	__tablename__ = 'Interval'
#CREATE TABLE Interval(id INTEGER PRIMARY KEY AUTOINCREMENT, `item` TEXT, seconds INTEGER);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_seconds = Column( 'Seconds', Integer )

	def __init__(self, item, seconds):
		self._item = item
		self._seconds = seconds

class Settings(Base):
	__tablename__ = 'Settings'
#CREATE TABLE Settings(id INTEGER PRIMARY KEY AUTOINCREMENT, `item` TEXT, `setting` TEXT);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_setting = Column( 'Setting', String(32) )

	def __init__(self, item, setting):
		self._item = item
		self._setting = setting

class Humi(Base):
	__tablename__ = 'Humidity'
	_id = Column('id', Integer, primary_key = True)
	_humi = Column('Humidity', Float)
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )
#CREATE TABLE Humidity(id INTEGER PRIMARY KEY AUTOINCREMENT, humidity FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);

	def __init__(self, humi):
		self._humi = humi

class Temp(Base):
	__tablename__ = 'Temperature'
#CREATE TABLE Temperature(id INTEGER PRIMARY KEY AUTOINCREMENT, temperature FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);
	_id = Column('id', Integer, primary_key = True)
	_temp = Column( 'Temperature', Float )
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, temp, curTime = None, uuid = ''):
		self._temp = temp
		if curTime is not None:
			self._curdate = curTime

		self._uuid = uuid

class POnOff(Base):
	__tablename__ = 'POnOff'
#CREATE TABLE POnOff(id INTEGER PRIMARY KEY AUTOINCREMENT, Item TEXT, pStatus TEXT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), UUID TEXT);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_pstatus = Column( 'pStatus', String(32) )
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, item, pStatus):
		self._item = item
		self._pstatus = pStatus

class FanPower(Base):
	__tablename__ = 'FanPower'
	_id = Column('id', Integer, primary_key = True)
	_temp = Column('Temperature', Integer )
	_fanpower = Column('FanPower', Integer)

class Note(Base):
	__tablename__ = 'Note'
	_id = Column('id', Integer, primary_key = True)
	_datatype = Column( 'datatype', String(32) )
	_note = Column( 'Note', String(256) )
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, note, curdate, datatype = 'Note' ):
		self._datetype = datatype
		self._note = note
		self._curdate = curdate

