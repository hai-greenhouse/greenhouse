import RPi.GPIO as GPIO
from time import sleep
from clog import clog
import configparser, os

class tb6600():
	def __init__( self ):
		config = configparser.ConfigParser()
		config.read( os.path.abspath( os.getcwd() ) + '/hAI.ini' )

		GPIO.setmode(GPIO.BCM)
		GPIO.setwarnings(False)
		self._ena_pin = int(config['Shade']['ENA_GPIO_PIN'])
		self._dir_pin = int(config['Shade']['DIR_GPIO_PIN'])
		self._pul_pin = int(config['Shade']['PUL_GPIO_PIN'])
		self._zero_pin = int(config['Shade']['ZERO_GPIO_PIN'])
		self._delay = 0.0005
		self._cw = 1
		self._ccw = 0
		self._coverstep = int(config['Shade']['COVERSTEP'])
		self._shadestatus = 0

		GPIO.setup( self._dir_pin, GPIO.OUT )
		GPIO.setup( self._pul_pin, GPIO.OUT )
		GPIO.setup( self._ena_pin, GPIO.OUT )
		GPIO.setup( self._zero_pin, GPIO.IN )

		self.Zeroing()
	
	def ShadeOn( self ):

		if self._shadestatus == 0:
			GPIO.output( self._dir_pin, self._cw )
			for x in range( self._coverstep ):
				GPIO.output( self._pul_pin, GPIO.HIGH )
				sleep( self._delay )
				GPIO.output( self._pul_pin, GPIO.LOW )
				sleep( self._delay )

			self._shadestatus = 1
			clog( "  [TB6600] ShadeOn:success" )
		else:
			clog( "  [TB6600] ShadeOn:status:0, do nothing" )


	def ShadeOff( self ):

		if self._shadestatus == 1:
			GPIO.output( self._dir_pin, self._ccw )
			for x in range( self._coverstep ):
				GPIO.output( self._pul_pin, GPIO.HIGH )
				sleep( self._delay )
				GPIO.output( self._pul_pin, GPIO.LOW )
				sleep( self._delay )

			self._shadestatus = 0
			clog( "  [TB6600] ShadeOff:success" )
		else:
			clog( "  [TB6600] ShadeOn:status:1, do nothing" )

	def Zeroing( self ):

		return True

		GPIO.output( self._dir_pin, self._ccw )
		count = 0
		while GPIO.input( self._zero_pin ) == 0:
			GPIO.output( self._pul_pin, GPIO.HIGH )
			sleep( self._delay )
			GPIO.output( self._pul_pin, GPIO.LOW )
			sleep( self._delay )
			count += 1

			if count > 60000:
				clog( "  [TB6600] Zeroing:failed:", count )
				return False
			
		self._shadestatus = 0
		clog( "  [TB6600] Zeroing:success:", count )
		return True

