from threading import Thread, Event
import RPi.GPIO as GPIO
import time
from enum import Enum
from clog import clog

# 光的執行模式
# 每 30 分鐘抓一次光，達到標準就切換，切換後 30 分鐘不會動。

class SensorMode(Enum):
	Above = 0
	Under = 1

class cHumi(Thread):
	def __init__(self, event, pin, objSensor):
		Thread.__init__(self)
		self._pin = pin 
		self.stopped = event
		GPIO.setmode( GPIO.BCM )
		GPIO.setup( self._pin, GPIO.OUT )
		GPIO.output( self._pin, GPIO.HIGH )
		self._objsensor = objSensor
		self._interval = 300
		self._exectime = 90
		self._humiHigh = 70
		self._humiLow = 70
		self._powerstatus = 0
		self._humiMode = 0
		self._humi = None

	def getHumiNow(self):
		if self._humi is None:
			self._humi = self._objsensor.getHumi()

		return self._humi

	def setONOFF(self, HIGH, LOW):
		self._tempHigh = HIGT
		self._tempLow = LOW

	def setGPIOMode(self, gpiomode):
		GPIO.setmode( gpiomode )

	def setInterval(self, seconds):
		self._interval = seconds

	def setObj(self, obj ):
		self._ob = obj

	def PON(self, first = None ):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.LOW )
			clog( "  [cHumi] cHumi:PON " )
			if first is None and self._ob['dbh']:
				self._ob['dbh'].inHumiStatus( "ON" )

			self._powerstatus = 1
			self._ob['lcd'].HumiOnOff('ON')

	def POFF(self, first = None ):
		if self._pin is not None:
			GPIO.output( self._pin, GPIO.HIGH )
			clog( "  [cHumi] cHumi:POFF " )
			if first is None and self._ob['dbh']:
				self._ob['dbh'].inHumiStatus( "OFF" )

			self._powerstatus = 0
			self._ob['lcd'].HumiOnOff('OFF')

	def firstrun(self):
		self._ob['lcd'].show("Humi testing...", 1)
		self.PON( first = True )
		time.sleep(5)
		self.POFF( first = True )
		self.updateInterval()

	def getHumiMode(self):
		return self._humiMode

	def HumiMode(self, mode='auto'):
		if mode == 'manual':
			self._ob['dbh'].inHumiMode( 1 )
			self._humiMode = 1
			self.PON()
		else:
			self._ob['dbh'].inHumiMode( 0 )
			self._humiMode = 0
			self._interval = 60
			self.POFF()

	def getPowerStatus(self):
		return self._powerstatus

	def getHumiFromDB(self, hour):
		try:
			(self._humiLow, self._humiMode) = self._ob['dbh'].getHumiOn( hour )
		except Exception as e:
			clog("[Error] [cHumi] getHumiFromDB Error ", str(e) )

	def setGPIO(self, PIN):
		self._pin = PIN

	def CleanUP(self):
		GPIO.cleanup()

	def updateInterval(self, item = 'cHumi'):
		try:
			sec = self._ob['dbh'].getInterval( item )
			if type( sec ) == int:
				self._interval = sec
				return sec
		except Exception as e:
			self._interval = 300
			clog("[Error] [cHumi] updateInterval:", str(e) )

		return False

	def run(self):
		while not self.stopped.wait(self._interval):
			self._humi = self._objsensor.getHumi()
			self.getHumiFromDB( int(time.strftime( '%H', time.localtime() ) ) )
			clog( "  [cHumi] run:Humi:", self._humi, " HumiLow:", self._humiLow )

			self.updateInterval( 'cHumi' )
			if ( self._humiMode == 1 ):
				clog( "  [cHumi] humiMode = 1, (Manual ON), skip auto")
				continue

			if self._powerstatus == 1:
				self.POFF()
				#sec = self._interval
				#self.updateInterval( 'cHumi' )
				#self._interval = self._interval - sec
				clog( "  [cHumi] [Info] updateInterval 'cHumi', Next Exec: %ds" % self._interval )
				continue

			if self._humi < self._humiLow and self._powerstatus == 0:
				if self._ob['dbh'].getSettings( 'cHumiCloseDoor' ) == '1':
					self._ob['cDoor'].closeDoor()
					clog("	[cHumi] [Info] cHumiCloseDoor:1, exec cDoor:closeDoor()" )

				self.PON()
				self.updateInterval( 'execHumi' )
				clog( "  [cHumi] [Info] updateInterval 'execHumi', Next exec:%ds" % self._interval ) 

