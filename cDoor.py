from threading import Thread, Event
import time
import m28BYJ_48
from clog import clog

class cDoor(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self._motor = m28BYJ_48.m28BYJ_48()
        self.stopped = event
        self._interval = 1800
        self._DoorOnOff = 0
        self._HumiCloseDoor = 0
        self._DoorStatus = 0
        self._running = 0

    def setObj(self, obj):
        self._ob = obj

    def getDoorOnOff(self, hour):
        self._DoorOnOff = self._ob['dbh'].getDoorOnOff( hour );
        clog( " [cDoor] [Info]:", self._DoorOnOff, " Hour:", hour )

    def getHumiCloseDoor(self):
        try:
            self._HumiCloseDoor = self._ob['dbh'].getSettings( 'cHumiCloseDoor' )
        except:
            clog( "[Error] [cDoor] getHumiCloseDoor:", str(e) )

    def updateInterval(self, item = 'cDoor'):
        try:
            sec = self._ob['dbh'].getInterval( item )
            if type( sec ) == int:
                self._interval = sec
                return sec
        except Exception as e:
            clog( "[cDoor] [Error] updateInterval:", str(e) )
            self._interval = 1800

        return False

    def firstrun(self):
        self._ob['lcd'].show("Door testing...", 1)
        self.openDoor();
        time.sleep(3)
        self.closeDoor();
        return True

    def openDoor(self):
        clog(" [cDoor] [Info] openDoor" )
        self._running = 1
        self._motor.set_motor('0000')
        self._motor.forward( 360, 0.01 )
        self._DoorStatus = 1
        self._running = 0

    def closeDoor(self):
        clog(" [cDoor] [Info] closeDoor" )
        self._running = 1
        self._motor.set_motor('0000')
        self._motor.reverse( 360, 0.01 )
        self._DoorStatus = 0
        self._running = 0

    def run(self):
        while not self.stopped.wait(self._interval):
            self.getDoorOnOff( int(time.strftime( '%H', time.localtime() ) ) )
            clog(" [cDoor] [Info] cDoor:DoorOnOff:",  self._DoorOnOff, " DoorStatus:", self._DoorStatus )

            if self._running == 1:
                clog(" [cDoor] cDoor:running:", self._running, " skip this time")
                continue

            if self._DoorOnOff == 1 and self._DoorStatus == 0:
                self.openDoor()

            if self._DoorOnOff == 0 and self._DoorStatus == 1:
                self.closeDoor()

