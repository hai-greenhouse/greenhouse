<div class="center">

## Main
```mermaid
flowchart LR

    system[systemctl start hAI] --> main[./hAI-main.py]
    main --> senLight[thread,senLight] -->|include| max44009.py
    main --> senTemp[thread,senTemp] -->|include| SHT[sht30.py or dht22.py]
    main --> senHumi[thread,senHumi]
    main --> cLight[thread,cLight.py] -->|include| senLight
    main --> cTemp[thread,cTempture.py] -->|inlcude| senTemp
    main --> cHumi[thread,cHumi.py] -->|inlcude| senHumi
    main --> cDoor[thread,cDoor.py] -->|inlcude| m28BYJ_48.py
    main --> thLCD[thread,cLCD.py] -->|include| LCD1602.py
    main --> cWebSocket[thread,cWebsocket.py]
    main --> dbh[object,dbh] -->|inlclude| cSQLAlchemy.py
    main --> cLog[object,clog.py]
    senHumi --> senTemp
```

</div>

## Threads

```mermaid
flowchart LR

    subgraph cc["c*.py"]
        la["cTemp.objsensor"]
        lb["cHumi.objsensor"]
        li["*._interval"]
        lr["*.run()"]
        ll["cLight.objsensor"]
    end
    la --> tc
    li -->|argv=object_name| dg
    lr -->|_interval| lr
    ll --> te
    lb -..-> td

    subgraph Sen["sen*"]
        ta["senTemp.getTemp()"]
        tb["senHumi.getHumi()"]
        tc["senTemp"]
        td["senHumi"]
        te["senLight"]
        ti["*._interval"]
        tg["senLight.getLumi()"]
        tr["*.run()"]
    end
    tr -->|_interval| tr
    ti -->|argv=sen_name| dg
    tg --> ma
    tc --> sht30
    td -..-> tc
    te --> max44009
    ta --> sa
    tb -..-> sd

    subgraph max44009
        ma[".luminosity()"]
        mb[".interval"]
        mc[".run()"]
    end
    mb -->|argv=senLight| dg
    mc -->|_interval| mc

    subgraph sht30["sht30.py"]
        sa[".getTemp()"]
        sb[".interval"]
        sc[".run()"] 
        sd[".getHumi()"]     
    end
    sb -->|argv=senTemp| dg
    sb -->|argv=senHumi| dg
    sc -->|_interval| sc

    subgraph dbh
        dg["cSQLAlchemy.getInterval( argv )"] --> table,Interval:item
    end
```
