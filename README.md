# hAI GreenHouse

This project was made by Python 3, Javascript(Jquery) and HTML.
- Turn On/Off light based on lumi.
- Turn On/Off shade based on lumi.
- Turn On/Off fan based on temperature and it supports PWM.
- Turn On/Off humidifier based on Humi sensor.
- Lumi/Temperature/Humi visualization.
- Support turn On/Off light, fan, humi manual.

## Requirements

### Hardware

* DHT22 or SHT30
* MAX44009
* 4 way relay
* DC 12V and 5V Power
* Raspberry Pi 3B+
* LCD1602 (I2C)
* TB6600
* A Shell (Option)

### Software
* Python 3.x
* SQLite3

## Install
### Hardware

Please ref to this Youtube.

### Software

There are 2 systems in GitLab repo, frontend and backend.

- Backend: [hAI-GreenHouse/greenhouse](https://gitlab.com/hai-greenhouse/greenhouse).
- Frontend: [hAI-GreenHouse/frontend](https://gitlab.com/hai-greenhouse/frontend).

If you don't need Web GUI, you can just clone the backend(`greenhouse`),

#### Backend

Install packages
```
sudo apt-get install sqlite3 python3 python3-pip git python3-libgpiod
sudo apt-get install libatlas-base-dev libjasper-dev libhdf5-dev libopencv-dev python3-opencv
```

Install Python Modules
```
sudo pip3 install adafruit-circuitpython-dht Adafruit_DHT greenponik-bh1750
sudo pip3 install websocket-client websocket_server smbus flask-sqlalchemy configparser

sudo pip3 install picamera opencv-python numpy imutils

sudo pip3 install pyecharts flask websocket-server websocket-client rel flask-sqlalchemy

sudo pip3 install opencv-contrib-python
```

Download from GitLab, following command will save code in `/home/hAI`, if you want save in another path, please change path youself in the follow commands. 
```
sudo git clone https://gitlab.com/hai-greenhouse/greenhouse.git /home/hAI
```

**Change owner and work directory**
```
cd /home/hAI
```

Create Database
```
cat db.schema | sudo sqlite3 data.db
```

Enable I2C
```
sudo raspi-config

Interface Options > I2C > Enable
```

Install systemctl
```
sudo cp hAI.service /usr/lib/systemd/system
sudo systemctl daemon-reload
sudo systemctl start hAI
```

If everything works, enable the service when boot.
```
sudo systemctl enable hAI
```

Make the backup
```
mkdir /home/hAI/backup
crontab -e
1 0 * * * cp /home/hAI/data.db /home/hAI/backup/$(date '+%F_%H%M%S_data.db') 
```


#### Frontend

Install packages * Python modules
```
sudo apt-get install apache2 libapache2-mod-wsgi-py3
sudo pip3 install pyecharts flask websocket-server websocket-client rel flask-sqlalchemy
```

Git clone form GitLab:
```
sudo git clone https://gitlab.com/hai-greenhouse/frontend.git /var/www/html/v-hAI
```

Apache configuration:
```
sudo vi /etc/apache2/sites-available/000-default.conf

<VirtualHost *:80>

        ServerAdmin webmaster@localhost
        ServerName localhost
        DocumentRoot /var/www/html/v-hAI

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        WSGIDaemonProcess hAI user=www-data group=www-data threads=5
        WSGIScriptAlias / /var/www/html/v-hAI/hAI.wsgi

        <Directory /var/www/html/v-hAI>
                Options -Indexes +FollowSymlinks
                WSGIProcessGroup hAI
                WSGIApplicationGroup %{GLOBAL}
                Require all granted
        </Directory>

</VirtualHost>

```

Start Apache2
```
sudo systemctl restart apache2
```

#### Expose website to public

** Beware, Internet are dangerous! **

A Domain
You need a domain to expose your website, if you don't have one. You can use ours.
```
user2.iplant.tw
...
user99.iplant.tw
```
The domain names are free and shared so if someone took, please try next. You can patse the domain in browse, if website is not found, it's available.

Generate a SSL certificate
After you **have a domain**, please generate a ssl certificate.

Here we generate a self-sign SSL, I recommand you buy a SSL or use let's encrypt (certbot).
```
cd /home/hAI/tunnel
openssl req -x509 -nodes -newkey rsa:2048 -sha256 -keyout client.key -out client.crt
...
...
Common Name (e.g. server FQDN or YOUR name) []:(type-in domain here)
```
Once completed, there are 2 files (`cliet.key` and `client.crt` under `tunnel` folder).

Start expose you website.
```
vi /home/hAI/tunnel.service

  auth: (chage username and password here, DO NOT using default setting)
  host: (type-in domain here)

sudo cp /home/hAI/tunnel/tunnel.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start tunnel.service
sudo systemctl enable tunnel.service
```
