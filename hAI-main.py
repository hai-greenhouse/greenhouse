import Adafruit_DHT 
import RPi.GPIO as GPIO
import sys, time, json, configparser, os
import threading
import sqlite3
from websocket_server import WebsocketServer

import max44009, bh1750, dht22, sht30
import cLight, cTempture, cHumi, cLCD, cDoor, cWebSocket, cSQLAlchemy
from clog import clog

def main():

	config = configparser.ConfigParser()
	config.read( os.path.abspath( os.getcwd() ) + '/hAI.ini' )

	try:
		stopFlag = threading.Event()
		#dbh = cDB.cDB()
		dbh = cSQLAlchemy.cDB()
		thLCD = cLCD.LCD16xx( stopFlag )
		ob = {}
		ob['lcd'] = thLCD
		ob['dbh'] = dbh
		ob['Websocket'] = config['WebSocket']['Enable']

		clog(" Init")
		if config['Door']['Enable'] == '1':
			clog( " [Door] Door:Enable:1")
			thDoor = cDoor.cDoor(stopFlag)
			ob['cDoor'] = thDoor
			thDoor.setObj( ob )

		if config['Light']['Enable'] == '1':
			if config['Module']['LIGHT'] == 'BH1750':
				clog( " [Module] Light:BH1750")
				senLight = bh1750.BH_1750(stopFlag)
			else:
				clog( " [Module] Light:MAX44009")
				senLight = max44009.MAX44009(stopFlag, 1, 0x4a)

			senLight.configure(cont=0, manual=0, cdr=0, timer=0)
			senLight.setObj( ob )

			thLight = cLight.cLight(stopFlag, int(config['Light']['RELAY_GPIO_PIN']), senLight)
			thLight.setObj( ob )
			ob['cLight'] = thLight

		if config['Temperature']['Enable'] == '1':
			if config['Module']['TEMP'] == 'DHT22':
				clog( " [Module] Temp:DHT22")
				senTemp = dht22.DHT22( stopFlag, int(config['DHT22']['GPIO_PIN']) )
			else:
				clog( " [Module] Temp:SHT30")
				senTemp = sht30.SHT30( stopFlag )

			senHumi = senTemp
			senTemp.setObj( ob )
			thTemp = cTempture.cTemp(stopFlag, int(config['Temperature']['RELAY_GPIO_PIN']), int(config['Temperature']['GPIO_PWM_PIN']), senTemp)
			thHumi = cHumi.cHumi(stopFlag, int(config['Humidity']['RELAY_GPIO_PIN']), senTemp)
			thTemp.setObj( ob )
			thHumi.setObj( ob )
			ob['cTemp'] = thTemp
			ob['cHumi'] = thHumi
	
	
		if config['Light']['Enable'] == '1':
			clog( " [Module] Light:Enable:1")
			senLight.firstrun()
			senTemp.firstrun()			# Temp and Humi
			thLight.firstrun()
		if config['Temperature']['Enable'] == '1':
			clog( " [Module] Temperature:Enable:1")
			thTemp.firstrun()
			thHumi.firstrun()
		if config['Door']['Enable'] == '1':
			clog( " [Module] Door:Enable:1")
			thDoor.firstrun()
	
		if config['Light']['Enable'] == '1':
			senLight.start()
			thLight.start()

		if config['Temperature']['Enable'] == '1':
			senTemp.start()
			thTemp.start()
			thHumi.start()

		if config['Door']['Enable'] == '1':
			thDoor.start()
	
		thLCD.start()
		thLCD.rS0()
		clog( "...AP Start [OK]" )

		if config['WebSocket']['Enable'] == '1':
			thWS = cWebSocket.cWebSocket( stopFlag )
			thWS.setObj( ob )
			ob['ws'] = thWS
			clog(" ...WebSocket Server [OK]")
			thWS.start()
		
	except KeyboardInterrupt:
		thLight.CleanUP()
		thTemp.CleanUP()
		thHumi.CleanUP()
		thLCD.CleanUP()

		stopFlag.set()
		thLight.join()
		thTemp.join()
		thHumi.join()
		thLCD.join()
		thDoor.join()
		print( "Ctrl-C exit()" )

if __name__ == '__main__':
	main()

