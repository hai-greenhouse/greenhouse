import smbus
import time
from threading import Thread
from clog import clog

class SHT30(Thread):

	def __init__(self, event, bus=1, addr=0x44):
		Thread.__init__(self)
		self.stopped = event
		self._bus = smbus.SMBus(bus)
		self._addr = addr
		self._humi = 0
		self._temp = 0
		self._interval = 600

	def setInterval(self, INTER = 600):
		self._interval = INTER

	def setObj(self, obj):
		self._ob = obj

	def Read(self):
		retry = 0
		try:
			self._bus.write_i2c_block_data(self._addr, 0x2C, [0x06])
			time.sleep(0.8)
			data = self._bus.read_i2c_block_data(0x44, 0x00, 6)
			self._temp = ((((data[0] * 256.0) + data[1]) * 175) / 65535.0) - 45
			self._humi = 100 * (data[3] * 256 + data[4]) / 65535.0
			return self._humi, self._temp
		except:
			return self._humi, self._temp


	def getHumi(self):
		self.Read()
		return self._humi

	def firstrun(self):
		self.Read()
		self._ob['lcd'].wTemp( self._temp )
		self._ob['lcd'].wHumi( self._humi )

	def getTemp(self):
		if self._temp is None:
			self.Read()
		return self._temp

	def updateInterval(self, item):
		try:
			sec = self._ob['dbh'].getInterval( item )
			if type( sec ) == int:
				self._interval = sec
				return sec
		except Exception as e:
			clog("[SHT30] [Error] updateInterval, item:" , item )
		return False

	def run(self):
		while not self.stopped.wait(self._interval):
			self.Read()
			clog( "  [SHT30:run] Temp:" ,self._temp , " Humi:", self._humi )

			try:
				self._ob['dbh']
				self.WriteTemp2DB()
				self.WriteHumi2DB()
			except:
				clog( "[Error] SHT30:run, DBH not defined" )

			self._ob['lcd'].wTemp( self._temp )
			self._ob['lcd'].wHumi( self._humi )
			self.updateInterval( 'senTemp' )

	def WriteTemp2DB(self):
		clog( "  [SHT30] [Info] SHT30:WriteTemp2DB" )
		if self._temp is not None and self._ob['dbh'] is not None:
			self._ob['dbh'].inTemp( self._temp )

	def WriteHumi2DB(self):
		clog( "  [SHT30] [Info] SHT30:WriteHumi2DB")
		if self._humi is not None and self._ob['dbh'] is not None:
			self._ob['dbh'].inHumi( self._humi )

